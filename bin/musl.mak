all: musl.tar

musl.tar:
	# Configue
	mkdir -p musl/install
	rm -rf musl/install/*
	cd musl && ./configure --prefix=/usr
	
	# Build
	cd musl && make
	
	#Install
	cd musl && ./configure --prefix=./install
	cd musl && make install
	
	cd musl/install && tar -cf ../../musl.tar *
