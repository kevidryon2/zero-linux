all: busybox

busybox:
	-patch bb/Makefile -N < bb.patch
	cd bb && make defconfig && make
	cp bb/busybox .
