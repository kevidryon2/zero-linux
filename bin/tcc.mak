all: tcc.tar

tcc.tar:
	cd tinycc && ./configure --config-musl --prefix=/usr      --extra-cflags="-nodefaultlibs -static -L../../build/fs/usr/lib/ -lc"
	-patch tinycc/Makefile -N < tcc.patch
	cd tinycc && make
	
	mkdir -p tinycc/install
	rm -rf tinycc/install/*
	cd tinycc && ./configure --config-musl --prefix=./install --extra-cflags="-nodefaultlibs -static -L../../build/fs/usr/lib/ -lc"
	cd tinycc && make install
	
	cd tinycc/install && tar -cf ../../tcc.tar *
