BIN_EXECS = arch chgrp date ed getopt iostat ln mknod mv printenv rmdir setserial tar watch ash \
            chmod dd egrep grep ipcalc login mktemp netstat ps rpm sh touch zcat base32 chown df \
            false gunzip kbd_mode ls more nice pwd run-parts sleep true base64 conspy dmesg \
            fatattr gzip kill lsattr mount pidof reformime scriptreplay stat umount busybox cp \
            dnsdomainname fdflush hostname link lzop mountpoint ping resume sed stty uname cat \
            cpio dumpkmap fgrep hush linux32 makemime mpstat ping6 rev setarch su usleep chattr \
            cttyhack echo fsync ionice linux64 mkdir mt pipe_progress rm setpriv sync vi

SBIN_EXECS = acpid depmod fsck hwclock insmod iprule lsmod mkfs.minix pivot_root run-init swapoff \
             tunctl adjtimex devmem fsck.minix ifconfig ip iptunnel makedevs mkfs.vfat poweroff \
             runlevel swapon udhcpc arp fbsplash fstrim ifdown ipaddr klogd mdev mkswap \
             raidautorun setconsole switch_root uevent blkid fdisk getty ifenslave iplink loadkmap \
             mkdosfs modinfo reboot slattach sysctl vconfig blockdev findfs halt ifup ipneigh \
             logread mke2fs modprobe rmmod start-stop-daemon syslogd watchdog bootchartd \
             freeramdisk hdparm init iproute losetup mkfs.ext2 nameif route sulogin tc zcip

USRBIN_EXECS = [ bzip2 crontab du fgconsole head less lzma nohup pkill resize setuidgid sort \
               taskset traceroute unix2dos vlock xargs [[ cal cryptpw dumpleases find hexdump \
               logger man nproc pmap rpm2cpio sha1sum split tcpsvd traceroute6 unlink volname xxd \
               ascii chpst cut eject flock hexedit logname md5sum nsenter printf runsv sha256sum \
               ssl_client tee truncate unlzma w xz awk chrt dc env fold hostid lpq mesg nslookup \
               pscan runsvdir sha3sum strings telnet ts unshare wall xzcat basename chvt deallocvt \
               envdir free id lpr microcom od pstree rx sha512sum sum test tty unxz wc yes bc \
               cksum diff envuidgid ftpget install lsof mkfifo openvt pwdx script showkey sv tftp \
               ttysize unzip wget beep clear dirname expand ftpput ipcrm lspci mkpasswd passwd \
               readlink seq shred svc time udhcpc6 uptime which blkdiscard cmp dos2unix expr fuser \
               ipcs lsscsi nc paste realpath setfattr shuf svok timeout udpsvd users who bunzip2 \
               comm dpkg factor groups killall lsusb nl patch renice setkeycodes smemcap tac top \
               unexpand uudecode whoami bzcat crc32 dpkg-deb fallocate hd last lzcat nmeter pgrep \
               reset setsid softlimit tail tr uniq uuencode whois cc

USRSBIN_EXECS = addgroup brctl crond dnsd fdformat i2cdetect i2ctransfer loadfont nandwrite \
                partprobe rdev rtcwake svlogd ubidetach ubirsvol add-shell chat delgroup \
                ether-wake fsfreeze i2cdump ifplugd lpd nbd-client popmaildir readahead sendmail \
                telnetd ubimkvol ubiupdatevol adduser chpasswd deluser fakeidentd ftpd i2cget \
                inetd mim nologin powertop readprofile setfont tftpd ubirename udhcpd arping \
                chroot dhcprelay fbset httpd i2cset killall5 nanddump ntpd rdate remove-shell \
                setlogcons ubiattach ubirmvol

BINS          = bin/musl.tar bin/busybox bin/tcc.tar

CFLAGS += -nodefaultlibs -Lbuild/fs/lib/ -lc -static

all: clean build/zerocd.iso

clean:
	rm -rf build/*

clean-bin:
	rm $(BINS)
	

fs:
	cp -r skel build/fs
	ln -sr build/fs/usr/bin build/fs/bin
	ln -sr build/fs/usr/lib build/fs/lib

build/zerocd.iso: clean initramfs
	mkdir build/mount
	mkdir build/boot
	
	# Generate boot fileystsem
	mkdir build/boot/isolinux
	cp    bin/bzImage       build/boot/vmlinuz
	cp    build/initrd.gz   build/boot/0
	cp -r conf/boot/*       build/boot/isolinux
	cp    bin/isolinux.bin  build/boot/isolinux
	
	mkisofs -o build/zerocd.iso \
	        -b isolinux/isolinux.bin \
	        -c isolinux/boot.cat \
	        -no-emul-boot \
	        -boot-load-size 4 \
	        -boot-info-table \
	        build/boot

initramfs: fs libc $(BIN_EXECS) $(SBIN_EXECS) $(USRBIN_EXECS) $(USRSBIN_EXECS) init
	cd build/fs && find . | cpio -o -H newc | gzip -9 -n > ../initrd.gz

%: src/bin/% busybox
	ln -sr ./build/fs/bin/busybox ./build/fs/bin/$@
	#$(CC) $< -o build/fs/bin/$@

%: src/sbin/% busybox
	ln -sr ./build/fs/bin/busybox ./build/fs/sbin/$@
	#$(CC) $< -o build/fs/sbin/$@
	
%: src/usr/bin/% busybox
	ln -sr ./build/fs/bin/busybox ./build/fs/usr/bin/$@
	#$(CC) $< -o build/fs/usr/bin/$@
	
%: src/usr/sbin/% busybox
	ln -sr ./build/fs/bin/busybox ./build/fs/usr/sbin/$@
	#$(CC) $< -o build/fs/usr/sbin/$@

init:
	$(CC) src/init/*.c -o build/fs/init $(CFLAGS)

libc: bin/musl.tar
	tar -xf bin/musl.tar -C build/fs/usr/

cc: bin/cc.tar
	tar -xf bin/tcc.tar  -C build/fs/usr/

bin/cc.tar:
	cd bin && make -f tcc.mak
	
bin/musl.tar:
	cd bin && make -f musl.mak

bin/busybox:
	cd bin && make -f busybox.mak

busybox: bin/busybox
	cp bin/busybox build/fs/bin/
