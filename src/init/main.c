#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>
#include <linux/reboot.h>
#include <sys/syscall.h>
#include <sys/mount.h>

int sh_pid;

void stop_system() {
	
	printf("Syncing filesystems...\n");
	sync();
	printf("Powering off.\n");
	
	//syscall(SYS_reboot, LINUX_REBOOT_MAGIC1, 0x28121969, LINUX_REBOOT_CMD_POWER_OFF, NULL);
	
}

int main() {
	printf("Hello! Welcome to 0-Linux!\n\n");
	
	printf("Mounting filesystems...\n");
	printf("PROC: ");
	if (mount("proc", "/proc", "proc", 0, NULL) < 0) printf("NOT OK\n"); else printf("OK\n");
	printf("DEV: ");
	if (mount("udev", "/dev", "devtmpfs", 0, NULL) < 0) printf("NOT OK\n"); else printf("OK\n");
	printf("PTS: ");
	if (mount("devpts", "/dev/pts", "devpts", 0, NULL) < 0) printf("NOT OK\n"); else printf("OK\n");
	printf("SYS: ");
	if (mount("sysfs", "/sys", "sysfs", 0, NULL) < 0) printf("NOT OK\n"); else printf("OK\n");
	
	printf("Running the shell...\n");
	
	if (!(sh_pid = fork())) {
		execl("/bin/busybox", "sh", NULL);
		perror("ARGH! There has been an error in running the shell");
	}
	
	int sh_stat = 0;
	waitpid(sh_pid, &sh_stat, 0);
	
	if (WIFEXITED(sh_stat)) {
		if (WEXITSTATUS(sh_stat) != 0) {
			//Shell terminated abnormally
			printf("ERROR: Shell terminated abnormally! (Exit status: %d)\n", WEXITSTATUS(sh_stat));
			while (1);
		} else {
			printf("Shell terminated normally. The system will now be shut down.\n");
			stop_system();
		}
	}
	
	while (1) {
	}
}
