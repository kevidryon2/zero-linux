# ZeroCD Linux

ZeroCD is a minimal linux distro bootable off a CD.

## Building

On Debian/Ubuntu Linux:

```
sudo apt install genisoimage git gcc
git clone https://codeberg.org/kevidryon2/zero-linux.git --recursive
cd zero-linux
mkdir -p skel/{usr/{bin,sbin,lib,include},proc,root,sys,dev,etc,boot,sbin,tmp} build
make
```

The Makefile will automatically run the subrepos' build scripts on the first build, and all sequent builds will be much quicker.